export const fibb = (numberOfDigits: number) => {
  let num1 = 0;
  let num2 = 1;
  const digits = [];
  for (let i = 0; i < numberOfDigits; i++) {
    if (i === 0) {
      digits.push(num1);
    } else {
      digits.push(num2);
      // Aggregate
      const sum = num1 + num2;
      num1 = num2;
      num2 = sum;
    }
  }
  return digits.join(", ");
};

const main = () => {
  console.log(fibb(6));
};

if (require.main === module) {
  main();
}
